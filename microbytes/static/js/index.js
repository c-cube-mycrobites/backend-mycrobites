$(window).scrollTop(0);
$(document).ready(function() {

    $(".mic-sec-1").css("top","8%");

    $(window).scroll( function(){

        $(".mic-sec-1").css("top","3%");
    
        $('.mic-sec-2').each( function(i){
            
            var bottom_of_object = $('.sec2-heading').position().top + $('.sec2-heading').outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            if( bottom_of_window > bottom_of_object ){
                
                $('.mic-sec-2').fadeIn(1500);
                    
            }
            
        });

        $('.course-with-image').each( function(i){
            
            var bottom_of_object = $('.boy-image').position().top + $('.boy-image').outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
                 
            if( bottom_of_window > bottom_of_object ){
                
                $('.course-with-image').fadeIn(1500);
                    
            }
            
        });  
    
    });

    $(".searchbtn").click(function(){
        $(this).toggleClass("bg-green");
        $(".fas").toggleClass("color-white");
        $(".input").focus().toggleClass("active-width").val('');
    });

    $('#nav-contactUS').click(function () {
        $(".mic-sec-1").css("top","3%");
        $(".mic-sec-2").css("display","block");
        $('.course-with-image').css("display","block");
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height()});
    });

    $('.btn').click(function () {
        $(".mic-sec-2").css("display","block");
        $('.course-with-image').css("display","block");
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height()});
    });
});